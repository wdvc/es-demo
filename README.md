# es-demo

#### 介绍
springboot+es的简单使用demo

#### 软件架构
软件架构说明
springboot 2.7.12
es 7.6.1

二次更新
添加RestHighLevelClient操作

后续可能会添加restTemplate操作es
GET product-info/_search 查询数据
POST product-info/_doc  添加数据
PUT product-info/_doc/${id}  更新数据
POST product-info/_delete_by_query  删除数据
POST product-info/_bulk  批量操作


#### 安装的一些主要步骤

docker命令安装

先创建文件夹/mydata/elasticsearch/* 和文件并赋权限  chmod -R 777 /mydata/elasticsearch/

elasticsearch.yml内容

cluster.name: docker-cluster
network.host: 0.0.0.0
http.cors.enabled: true
http.cors.allow-origin: "*"

拉取镜像
docker pull elasticsearch:7.6.1
安装es
docker run --name es7.6 \
-p 9200:9200 -p 9300:9300 \
-e "discovery.type=single-node" \
-e ES_JAVA_OPTS="-Xms64m -Xmx512m" \
-v /mydata/elasticsearch/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml \
-v /mydata/elasticsearch/data:/usr/share/elasticsearch/data \
-v /mydata/elasticsearch/plugins:/usr/share/elasticsearch/plugins \
-d elasticsearch:7.6.1

安装IK分词器，支持模糊查询
https://blog.csdn.net/yangkei/article/details/127383630

安装 IK 分词器插件,版本要和es的保持一致
首先进入容器：
docker exec -it es7.6 /bin/bash
安装插件
./bin/elasticsearch-plugin install https://github.com/medcl/elasticsearch-analysis-ik/releases/download/v7.6.1/elasticsearch-analysis-ik-7.6.1.zip

es可视化：kibana
拉取镜像
docker pull kibana:7.6.1
安装启动
docker run --name kibana -e ELASTICSEARCH_HOSTS=http://172.17.0.4:9200 -p 5601:5601 -d kibana:7.6.1
进入容器
docker exec -it kibana得容器 /bin/sh

#使用vi 修改文件内容
vi /usr/share/kibana/config/kibana.yml

server.name: kibana
server.host: "0"
#elasticsearch.hosts: [ "http://elasticsearch:9200" ]
elasticsearch.hosts: [ "http://自己的elasticsearch的IP:9200" ]
xpack.monitoring.ui.container.elasticsearch.enabled: true
#设置kibana中文显示
i18n.locale: zh-CN

注意:
ELASTICSEARCH_HOSTS为docker镜像中查询到的es的ip地址，如果错误就会报错“Kibana server is not ready yet”
查看IP
docker inspect --format '{{ .NetworkSettings.IPAddress }}'  es容器ID
// 查看es容器id
docker ps

es可视化还有一个客户端：ElasticHD
如何使用：https://blog.csdn.net/feiying0canglang/article/details/126348055


#### 问题记录
启动报错：
org.elasticsearch.client.ResponseException: method [PUT], host [http://10.0.180.100:9200], URI [/productInfo], status line [HTTP/1.1 406 Not Acceptable]

es版本和springboot版本不一致导致，我最开始用的是springboot3和es7.6，后来把springboot版本降到了2.7.x;


org.elasticsearch.client.ResponseException: method [PUT], host [http://10.0.180.100:9200], URI [/productInfo?master_timeout=30s&timeout=30s], status line [HTTP/1.1 400 Bad Request]
{"error":{"root_cause":[{"type":"invalid_index_name_exception","reason":"Invalid index name [productInfo], must be lowercase","index_uuid":"_na_","index":"productInfo"}],"type":"invalid_index_name_exception","reason":"Invalid index name [productInfo], must be lowercase","index_uuid":"_na_","index":"productInfo"},"status":400}

@Document(indexName = "productInfo")，indexName不能使用大写，替换成了@Document(indexName = "product-info")


常用方法
https://blog.csdn.net/qq_43692950/article/details/122285770






