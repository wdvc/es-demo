package com.es.demo;

import com.es.demo.task.CronTaskRegistrar;
import com.es.demo.task.ScheduleRunnalbe;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class EsDemoApplicationTests {

    @Autowired
    private CronTaskRegistrar cronTaskRegistrar;
    @Test
    void contextLoads() {
        ScheduleRunnalbe scheduledTask = new ScheduleRunnalbe("0/3 * * * * ?","bussinessTask","test");
        cronTaskRegistrar.addTask(scheduledTask, "0/3 * * * * ?");

        ScheduleRunnalbe scheduledTask2 = new ScheduleRunnalbe("0/5 * * * * ?","bussinessTask","test","123");
        cronTaskRegistrar.addTask(scheduledTask2,"0/5 * * * * ?");

        try {
            Thread.sleep(60000L);
        } catch (Exception e) {

        }
        //动态修改定时任务执行时间
        ScheduleRunnalbe scheduledTask3 = new ScheduleRunnalbe("0/8 * * * * ?","bussinessTask","test","123");
        cronTaskRegistrar.addTask(scheduledTask3,"0/8 * * * * ?");

        try {
            Thread.sleep(10000000L);
        } catch (Exception e) {

        }
    }

}
