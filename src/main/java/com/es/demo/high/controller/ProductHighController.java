package com.es.demo.high.controller;

import com.es.demo.document.ProductInfo;
import com.es.demo.high.service.ProductHighService;
import com.es.demo.request.IdRequest;
import com.es.demo.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("productHigh")
public class ProductHighController {

    @Autowired
    private ProductHighService productService;

    @PostMapping("save")
    public ResponseEntity save(@RequestBody ProductInfo productInfo) {
        productInfo.setCreateTime(productInfo.getCreateTime() == null ? new Date() : productInfo.getCreateTime());
        productService.save(productInfo);
        return ResponseEntity.ok(null);
    }

    @PostMapping("delete")
    public ResponseEntity delete(@RequestBody IdRequest request) {
        productService.delete(request.getId());
        return ResponseEntity.ok(null);
    }

    @GetMapping("all")
    public ResponseEntity getAll() {
        List<ProductInfo> all = productService.getAll();
        return ResponseEntity.ok(all);
    }

    @GetMapping("selectById")
    public ResponseEntity getById(Integer id) {
        ProductInfo byId = productService.getById(id);
        return ResponseEntity.ok(byId);
    }

    @GetMapping("query")
    public ResponseEntity query(String keyword) {
        List<ProductInfo> query = productService.query(keyword);
        return ResponseEntity.ok(query);
    }
}
