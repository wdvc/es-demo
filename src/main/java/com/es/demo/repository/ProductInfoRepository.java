package com.es.demo.repository;

import com.es.demo.document.ProductInfo;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Component;

@Component
public interface ProductInfoRepository extends ElasticsearchRepository<ProductInfo, Integer> {
}
