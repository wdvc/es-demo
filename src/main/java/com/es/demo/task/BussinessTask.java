package com.es.demo.task;

import org.springframework.stereotype.Component;

@Component("bussinessTask")
public class BussinessTask {

    public void test() {
        System.out.println("这是定时任务test");
    }

    public void test(String aaa) {
        System.out.println("这是定时任务test（aaa）,aaa="+aaa);
    }
}
