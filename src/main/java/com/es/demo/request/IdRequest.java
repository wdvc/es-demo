package com.es.demo.request;

import lombok.Data;

@Data
public class IdRequest {

    private Integer id;
}
