package com.es.demo.service;

import com.es.demo.constant.enums.IndexEnum;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Map;

public interface BasicEsService<T> {

    /**
     * 保存数据
     *
     * @param indexEnum 索引
     * @param ts        数据
     */
    void save(IndexEnum indexEnum, T... ts);

    /**
     * 删除数据
     *
     * @param indexEnum 索引
     * @param id        id
     */
    void delete(IndexEnum indexEnum, String id);

    /**
     * 查询单个数据
     *
     * @param indexEnum
     * @param id
     * @param clazz
     * @return
     */
    T getById(IndexEnum indexEnum, String id, T clazz);

    /**
     * 查询所有数据，es限制最多返回10000条
     *
     * @param indexEnum
     * @param clazz
     * @return
     */
    List<T> getAllData(IndexEnum indexEnum, T clazz);

    /**
     * 动态sql查询
     *
     * @param sql
     * @param clazz
     * @return
     */
    List<Map<String, Object>> query(String sql, T clazz);

    /**
     * 范围查询
     *
     * @param pageNo
     * @param pageSize
     * @param indexEnum
     * @param order
     * @param rangeParam
     * @return
     */
    Page<T> rangeQuery(Integer pageNo, Integer pageSize, IndexEnum indexEnum, Map<String, String> order, Map<String, Object[]> rangeParam, T clazz);

    /**
     * 分页查询，默认十条，支持多字段模糊匹配，多字段排序
     *
     * @param pageNo   从0开始
     * @param pageSize 每页记录数
     * @param keyword  关键词
     * @param clazz    转换的对象
     * @param order    排序集合
     * @param fields   搜索的列
     * @return
     */
    Page<T> pageList(Integer pageNo, Integer pageSize, String keyword, T clazz, Map<String, String> order, String... fields);
}
