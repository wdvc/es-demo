package com.es.demo.service;

import com.es.demo.document.ProductInfo;

import java.util.List;

public interface ProductService extends BasicEsService<ProductInfo> {

    Boolean save(ProductInfo... productInfo);

    Boolean delete(Integer id);

    ProductInfo getById(Integer id);

    List<ProductInfo> getAll();

    List<ProductInfo> query(String keyword);
}
