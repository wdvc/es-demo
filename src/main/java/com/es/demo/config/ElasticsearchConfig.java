package com.es.demo.config;

import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableElasticsearchRepositories(basePackages = "com.es.demo.repository")
public class ElasticsearchConfig {

    @Value("${spring.elasticsearch.uris}")
    private String urls;


    @Bean
    public RestHighLevelClient geClient() {
        RestHighLevelClient client = null;
        String[] split = urls.split(":");
        HttpHost httpHost = new HttpHost(split[0], Integer.parseInt(split[1]), "http");
        client = new RestHighLevelClient(RestClient.builder(httpHost));
        return client;
    }
}
