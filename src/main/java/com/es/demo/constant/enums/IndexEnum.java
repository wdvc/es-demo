package com.es.demo.constant.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum IndexEnum {

    PRODUCT_INFO("product-info", "_doc");

    private String index;
    private String type;
}
